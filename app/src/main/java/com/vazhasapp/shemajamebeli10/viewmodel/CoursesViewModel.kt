package com.vazhasapp.shemajamebeli10.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.shemajamebeli10.model.Topic
import com.vazhasapp.shemajamebeli10.networking.ApiRepoImpl
import com.vazhasapp.shemajamebeli10.networking.ResponseHandler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CoursesViewModel @Inject constructor(private val apiRepoImpl: ApiRepoImpl
): ViewModel() {

    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private var _topicMutableLiveData = MutableLiveData<ResponseHandler<Topic>>()
    val topicMutableLiveData: LiveData<ResponseHandler<Topic>> get() = _topicMutableLiveData

    fun getTopic() {
        viewModelScope.launch {
            withContext(mainDispatcher) {
                apiRepoImpl.getTopic()
            }
        }
    }
}