package com.vazhasapp.shemajamebeli10.model

data class Courses(
    val background_color_precent: String,
    val color: String,
    val image: String,
    val precent: String,
    val title: String
)