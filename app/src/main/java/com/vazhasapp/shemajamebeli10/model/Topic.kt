package com.vazhasapp.shemajamebeli10.model

data class Topic(
    val color: String,
    val duration: Int,
    val title: String,
    val type: String
)