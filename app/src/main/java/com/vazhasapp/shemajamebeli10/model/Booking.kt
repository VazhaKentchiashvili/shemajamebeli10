package com.vazhasapp.shemajamebeli10.model

data class Booking(
    val cours: List<Courses>,
    val topic: List<Topic>
)