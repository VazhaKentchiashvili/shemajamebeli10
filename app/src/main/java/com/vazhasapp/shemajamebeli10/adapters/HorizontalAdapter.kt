package com.vazhasapp.shemajamebeli10.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli10.R
import com.vazhasapp.shemajamebeli10.databinding.MainItemCardViewBinding
import com.vazhasapp.shemajamebeli10.model.Topic

class HorizontalAdapter(private val topicList: MutableList<Topic>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val PAID_COURSE = 0
        const val FREE_COURSE = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == PAID_COURSE) {
            PaidItemViewHolder(
                MainItemCardViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            return FreeItemViewHolder(
                MainItemCardViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PaidItemViewHolder -> holder.bind()
            is FreeItemViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = topicList.size

    override fun getItemViewType(position: Int): Int {
        val currentTopic = topicList[position]

        return if (currentTopic.type == "free") {
            FREE_COURSE
        } else {
            PAID_COURSE
        }
    }

    inner class PaidItemViewHolder(private val binding: MainItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var currentTopic: Topic

        fun bind() {
            currentTopic = topicList[adapterPosition]

            binding.imPurchase.setImageResource(R.drawable.ic_card)
            binding.isPaidOrNot.text = currentTopic.type
            binding.tvWhatIsIt.text = currentTopic.title
            binding.tvDuration.text = currentTopic.duration.toString()
        }
    }

    inner class FreeItemViewHolder(private val binding: MainItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var currentTopic: Topic

        fun bind() {
            currentTopic = topicList[adapterPosition]

            fun bind() {
                binding.imPurchase.setImageResource(R.drawable.ic_free_course)
                binding.isPaidOrNot.text = currentTopic.type
                binding.tvWhatIsIt.text = currentTopic.title
                binding.tvDuration.text = currentTopic.duration.toString()
            }
        }
    }

    fun setData(enteredTopicList: MutableList<Topic>) {
        topicList.clear()
        enteredTopicList.addAll(enteredTopicList)
        notifyDataSetChanged()
    }
}