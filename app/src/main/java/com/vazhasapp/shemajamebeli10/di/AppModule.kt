package com.vazhasapp.shemajamebeli10.di

import com.vazhasapp.shemajamebeli10.networking.ApiRepo
import com.vazhasapp.shemajamebeli10.networking.ApiRepoImpl
import com.vazhasapp.shemajamebeli10.networking.ApiService
import com.vazhasapp.shemajamebeli10.utils.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun retrofitBuilder(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun apiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun apiRepoImpl(apiService: ApiService): ApiRepoImpl = ApiRepoImpl(apiService)
}