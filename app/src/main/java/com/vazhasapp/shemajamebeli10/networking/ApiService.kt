package com.vazhasapp.shemajamebeli10.networking

import com.vazhasapp.shemajamebeli10.model.Courses
import com.vazhasapp.shemajamebeli10.model.Topic
import com.vazhasapp.shemajamebeli10.utils.Constants.API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(API_ENDPOINT)
    suspend fun getTopic() : Response<Topic>

    @GET(API_ENDPOINT)
    suspend fun getCources() : Response<Courses>
}