package com.vazhasapp.shemajamebeli10.networking

import com.vazhasapp.shemajamebeli10.model.Courses
import com.vazhasapp.shemajamebeli10.model.Topic
import retrofit2.Response

interface ApiRepo {

    suspend fun getTopic(): ResponseHandler<Topic>

    suspend fun getCourses(): ResponseHandler<Courses>

}