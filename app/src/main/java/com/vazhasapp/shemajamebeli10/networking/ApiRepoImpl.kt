package com.vazhasapp.shemajamebeli10.networking

import android.widget.Toast
import com.vazhasapp.shemajamebeli10.model.Courses
import com.vazhasapp.shemajamebeli10.model.Topic
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ApiRepoImpl @Inject constructor(private val apiService: ApiService) : ApiRepo {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getTopic(): ResponseHandler<Topic> {
        return withContext(ioDispatcher) {
            try {
                val result = apiService.getTopic()

                if (result.isSuccessful) {
                    return@withContext ResponseHandler.Success(result.body())
                } else {
                    return@withContext ResponseHandler.Failure("Request Failed")
                }
            }catch (e: Exception) {
                return@withContext ResponseHandler.Failure("Request Failed")
            }
        }

    }


    override suspend fun getCourses(): ResponseHandler<Courses> {
        return ResponseHandler.Failure("Failed")
    }
}