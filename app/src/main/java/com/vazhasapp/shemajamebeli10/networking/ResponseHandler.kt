package com.vazhasapp.shemajamebeli10.networking

sealed class ResponseHandler<out T> {
    data class Success<out T>(val data: T?) : ResponseHandler<T>()
    data class Failure<T>(val errorMessage: String?, val data: T? = null) : ResponseHandler<T>()
}
